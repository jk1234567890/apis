package pl.edu.pw.mini.jk.apis

import java.io.{File => JFile}
import java.time.{DayOfWeek, LocalDate}
import scala.io.Source

import argonaut.JsonIdentity.ToJsonIdentity
import argonaut.Parse
import better.files.{File, FileOps}
import org.rogach.scallop.{ScallopConf, Subcommand}
import pl.edu.pw.mini.jk.apis.usos.Staff
import pl.edu.pw.mini.jk.apis.usos.models.Classes

class CmdOptions(arguments: Seq[String]) extends ScallopConf(arguments) {
  import Employee.eCodec

  implicit val dateConf = org.rogach.scallop
    .singleArgConverter(s => LocalDate.parse(s, Staff.dateFormatter))

  val employees = opt[JFile](required = true, short = 'e', name = "employee-list",
    argName = "file",
    descr = "Json file list of employees to generate table")
    .map(file => Parse.decodeEither[List[Employee]](Source.fromFile(file).mkString) match {
      case Right(list) => list
    })

  val cacheDir = opt[JFile](short = 'c', name = "cache-dir", default = Some(new JFile("cache")), descr = "Directory for caching data downlowaded from usos, default: ./cache/", argName = "dir")
  val downloadCommand = new Subcommand("download") {
    val dateFrom = opt[LocalDate](required = true, short = 'f', name = "from",
      descr = "First day of semester yyyy-mm-dd", argName = "date")
    val dateTo = opt[LocalDate](required = true, short = 't', name = "to",
    descr = "Last day of sem yyyy-mm-dd", argName = "date")
  }

  val generateCommand = new Subcommand("generateTable") {
    val texOutput = opt[JFile](required = true, short = 'o', name = "output", descr = "output latex file", argName = "file")
    val nameReplacements = opt[JFile](required = true, short = 'r', name = "name-replacements", descr = "Path to file with name replacements", argName = "file").map(Replacements.fromFile)
    val buildingReplacements = opt[JFile](required = true, short = 'b', name="building-replacements", descr = "Path to building name replacements", argName = "file").map(Replacements.fromFile)
  }

  addSubcommand(downloadCommand)
  addSubcommand(generateCommand)

  verify()
}

object Program {

  def formatClassForDisplay(building: String => String, cName: String => String)
    (c: FormattedClass) : String = {
    c.startTime.getHour+"--"+
    c.endTime.getHour+" "+cName(c.text)+
    " "+c.room
  }


  def main(args: Array[String]) : Unit = {
    val options = new CmdOptions(args)

    if(options.subcommands.contains(options.downloadCommand)) {
      import scala.language.reflectiveCalls

      val dateFrom = options.downloadCommand.dateFrom()
      val dateTo = options.downloadCommand.dateTo()
      val cacheDir : File = options.cacheDir().toScala.createDirectory()

      options.employees().foreach{emp =>
        val tt: List[Classes] = Staff.getTTForDateRange(emp.usosId, dateFrom, dateTo)
        (cacheDir/s"${emp.usosId}.json").overwrite(tt.asJson.toString())
      }
    }

    if(options.subcommands.contains(options.generateCommand)) {
      import scala.language.reflectiveCalls

      val cacheDir : File = options.cacheDir().toScala
      val rows = options.employees().map(emp => {
        val rawTt: List[FormattedClass] = emp.add.getOrElse(List()) ++
          (Parse.decodeEither[List[Classes]]((cacheDir/s"${emp.usosId}.json").contentAsString) match {case Right(a) => a})
          .filter(c => emp.remove.map(!_.exists(_.check(c))).getOrElse(true))
          .map(c => emp.appenders.map(_.foldLeft(c)((cl, a) => a.mangle(cl))).getOrElse(c))
          .map(_.format(options.generateCommand.buildingReplacements().replace))

        val tt: Map[DayOfWeek, Vector[FormattedClass]] = rawTt.groupBy(_.dayOfWeek).mapValues(_.sortBy(_.startTime).toVector)
        TTRecord(emp.name, emp.konsultacje, tt).getLatexRecord(options.generateCommand.nameReplacements(), options.generateCommand.buildingReplacements())
      })

      val sb = StringBuilder.newBuilder
      sb.append(Source.fromResource("pl/edu/pw/mini/jk/apis/prepend.tex").mkString)
      rows.foreach(sb.append _)
      sb.append(Source.fromResource("pl/edu/pw/mini/jk/apis/append.tex").mkString)
      options.generateCommand.texOutput().toScala.overwrite(sb.toString)
    }
  }
}
