package pl.edu.pw.mini.jk.apis

import java.io.File
import scala.io.Source


case class Replacements(
  rl: List[(String, String)]
) {
  def replace(input: String) : String = rl.foldLeft(input){case (str, (toRep, repl)) => str.replace(toRep, repl)}
}

object Replacements {
  def fromFile(file: File) : Replacements = Replacements.apply(
    Source.fromFile(file).getLines
      .map(s => s.split(";", 2) match { case Array(toRep, repl) => (toRep, repl)}).toList
  )
}
