package pl.edu.pw.mini.jk.apis

import java.time.DayOfWeek

case class TTRecord(personName: String, konsultacje: String, classes: Map[DayOfWeek, Vector[FormattedClass]]) {

  def tableHeight : Int =
    classes.values.map(_.size).foldLeft(1)(Ordering.Int.max(_, _))


  def getLatexRecord(classNameReplacements: Replacements, buildingReplacements: Replacements) = {
    def latexRow(num: Int) = {
      def classesRow = Seq.range(1, 6).map(DayOfWeek.of _).map(classes.get(_).flatMap(l => if(l.length <= num) None
      else Some(Program.formatClassForDisplay(buildingReplacements.replace _, classNameReplacements.replace _)(l(num))))
        .getOrElse("")).foldLeft("")(_ + "&" + _)
      def personRow = num match {
        case 0 => "\\multirow{"+tableHeight+"}{4.5cm}{"+personName+"}"
        case  _ => ""
      }
      def konsultacjeRow = num match {
        case 0 => "\\multirow{"+tableHeight+"}{3cm}{"+konsultacje+"}"
        case _ => ""
      }

      personRow + classesRow + "&" + konsultacjeRow + "\\\\ "
    }
    Seq.range(0,tableHeight).map(latexRow).foldLeft("")(_ + "\n" + _)+"\\hline"
 }
}
