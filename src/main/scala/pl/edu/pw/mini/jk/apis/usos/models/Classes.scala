package pl.edu.pw.mini.jk.apis.usos.models

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import argonaut.{CodecJson, DecodeJson, EncodeJson}
import pl.edu.pw.mini.jk.apis.FormattedClass

final case class Classes(name: Map[String, String], startTime: LocalDateTime, endTime: LocalDateTime, roomNumber: String, buildingId: String) {
  def format(buildingAbbrev: String => String): FormattedClass = {
    FormattedClass(name("pl"), startTime.getDayOfWeek, startTime.toLocalTime, endTime.toLocalTime, roomNumber++" "++buildingAbbrev(buildingId))
  }
}

object Classes {
  val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
  implicit val dateDecode : DecodeJson[LocalDateTime] = DecodeJson.StringDecodeJson.map(LocalDateTime.parse(_, dateFormatter))
  implicit val dateEncode : EncodeJson[LocalDateTime] = EncodeJson.StringEncodeJson.contramap((ld: LocalDateTime) => ld.format(dateFormatter))

  implicit val classesJsonDecode : CodecJson[Classes] = CodecJson.casecodec5(Classes.apply, Classes.unapply)("name", "start_time", "end_time", "room_number", "building_id")
}

//[{"end_time": "2017-03-03 12:00:00", "name": {"pl": "Projektowanie obiektowe - Laboratorium", "en": "Object Oriented Design - laboratory"}, "start_time": "2017-03-03 10:15:00", "building_id": "1120-MIN", "room_number": "302"}, {"end_time": "2017-03-03 16:00:00", "name": {"pl": "Projektowanie obiektowe - Laboratorium", "en": "Object Oriented Design - laboratory"}, "start_time": "2017-03-03 14:15:00", "building_id": "1120-MIN", "room_number": "217"}, {"end_time": "2017-03-06 17:00:00", "name": {"pl": "Systemy operacyjne 2 - Laboratorium", "en": "Operating Systems 2 - laboratory"}, "start_time": "2017-03-06 14:15:00", "building_id": "1120-MIN", "room_number": "203"}, {"end_time": "2017-03-07 14:00:00", "name": {"pl": "Algorytmy i struktury danych 2 - Laboratorium", "en": "Algorithms and Data Structures 2 - laboratory"}, "start_time": "2017-03-07 12:15:00", "building_id": "1120-MIN", "room_number": "301"}]
