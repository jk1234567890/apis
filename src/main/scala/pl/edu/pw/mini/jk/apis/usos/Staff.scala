package pl.edu.pw.mini.jk.apis.usos

import java.io.IOException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField
import pl.edu.pw.mini.jk.apis.usos.models.Classes

import argonaut.Parse
import scala.collection.immutable.TreeSet
import java.lang.RuntimeException

object Staff {

  val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
  

  def getTT(id: Int, from: LocalDate) : List[Classes] = {
    val result = try {
      scala.io.Source.fromURL("https://apps.usos.pw.edu.pl/services/tt/staff?user_id="+id+"&fields=start_time|end_time|name|room_number|building_id&start="+from.format(dateFormatter)).mkString
    } catch {
      case e: IOException if e.getMessage.contains("response code: 400") => {Console.err.println(s"Id not employee: $id"); "[]"}
    }
    implicit val decoder = Classes.classesJsonDecode
    Parse.decodeEither[List[Classes]](result) match {
      case Left(error) => throw new RuntimeException(error.toString())
      case Right(content) => content
    }
  }


  def getTTForDateRange(id: Int, from: LocalDate, to: LocalDate) : List[Classes] = {
    val repeated : Stream[Classes] = Stream.iterate(from)(_.plusWeeks(1)).takeWhile(to.isAfter(_)).flatMap(getTT(id,_))


    import Ordering._
    implicit val clOrd : Ordering[Classes] = Ordering.by((c: Classes) => (c.name.get("pl"), c.roomNumber, c.startTime.get(ChronoField.DAY_OF_WEEK), c.startTime.get(ChronoField.HOUR_OF_DAY)))
    (TreeSet[Classes]()++repeated).toList
  }

}
