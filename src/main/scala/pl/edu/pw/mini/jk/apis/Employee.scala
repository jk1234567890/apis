package pl.edu.pw.mini.jk.apis

import java.time.{DayOfWeek, LocalTime}
import java.time.format.DateTimeFormatter

import argonaut.{CodecJson, DecodeJson, EncodeJson}
import pl.edu.pw.mini.jk.apis.usos.models.Classes


final case class ClassRemover(namePart: String, dayOfWeek: DayOfWeek,  hour: Int) {
  def check(c: Classes) : Boolean = c.startTime.getDayOfWeek().equals(dayOfWeek) && c.startTime.getHour <= hour && c.endTime.getHour >= hour &&
  c.name.get("pl").map(_.contains(namePart)).getOrElse(false)
}

final case class NameAppender(append: String, namePart: String, dayOfWeek: Option[DayOfWeek], hour: Option[Int]) {
  def mangle(c: Classes) : Classes =
    if(
      dayOfWeek.map(_ equals(c.startTime.getDayOfWeek())).getOrElse(true) && hour.map(c.startTime.getHour <= _).getOrElse(true) &&
        hour.map(c.endTime.getHour >= _).getOrElse(true) &&
        c.name.get("pl").map(_.contains(namePart)).getOrElse(false)
    ) c.copy(roomNumber = c.roomNumber ++ " " ++ append)
    else c
}

final case class FormattedClass(text: String, dayOfWeek: DayOfWeek, startTime: LocalTime, endTime: LocalTime, room: String)

case class Employee(name: String, konsultacje: String, usosId: Int, remove: Option[List[ClassRemover]], add: Option[List[FormattedClass]], appenders: Option[List[NameAppender]])

object Employee {
  val timeFormatter = DateTimeFormatter.ofPattern("H:m")
  implicit val ltDecode : DecodeJson[LocalTime] = DecodeJson.StringDecodeJson.map(LocalTime.parse(_ : String, timeFormatter))
  implicit val ldEncode : EncodeJson[LocalTime] = EncodeJson.StringEncodeJson.contramap(_.format(timeFormatter))
  implicit val dowDecode: DecodeJson[DayOfWeek] = DecodeJson.IntDecodeJson.map(DayOfWeek.of _)
  implicit val dowEncode : EncodeJson[DayOfWeek] = EncodeJson.IntEncodeJson.contramap(_.getValue)

  implicit val adderCodec : CodecJson[FormattedClass] = CodecJson.casecodec5(FormattedClass.apply, FormattedClass.unapply)("text", "dayOfWeek", "startTime", "endTime", "room")
  implicit val removerCodec : CodecJson[ClassRemover] = CodecJson.casecodec3(ClassRemover.apply, ClassRemover.unapply)("namePart", "dayOfWeek", "hour")
  implicit val appenderCoded : CodecJson[NameAppender] = CodecJson.casecodec4(NameAppender.apply, NameAppender.unapply)("append", "namePart", "dayOfWeek", "hour")
  implicit val  eCodec : CodecJson[Employee] = CodecJson.casecodec6(Employee.apply, Employee.unapply)("name", "konsultacje", "usosId", "remove", "add", "append")
}
