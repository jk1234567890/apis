organization := "pl.edu.pw.mini.jk"
version := "0.1-SNAPSHOT"
scalaVersion := "2.12.3"
scalaVersion in ThisBuild := "2.12.3"
mainClass := Some("pl.edu.pw.mini.jk.apis.Program")
name := "apis"

libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.1.11"
libraryDependencies += "io.argonaut" %% "argonaut" % "6.2"
libraryDependencies += "org.rogach" %% "scallop" % "3.1.0"
libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.1.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

